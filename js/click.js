var k1 = document.getElementById('k1')
var k2 = document.getElementById('k2')
var k1Count = 0
var k2Count = 0
function k1Update () {
  k1.innerHTML = k1Count + 1
  k1Count += 1
}
function k2Update () {
  k2.innerHTML = k2Count + 1
  k2Count += 1
}
function keypress (event) {
  var key = event.which || event.keyCode
  if (key === 122) {
    k1Update()
  } else if (key === 120) {
    k2Update()
  }
}
